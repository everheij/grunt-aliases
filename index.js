var _ = require('lodash');

  // Due to jit-grunt's lazy loading tasks are recognized by grunt GUI tools such as sublime grunt
  // Create aliases for tasks to make them available
module.exports = function(grunt) {

  var configEntries = grunt.config.getRaw();
  var aliasses = [];
  _.forOwn(configEntries, function (entry, level1Key) {
    if ( ! _.isPlainObject(entry) ) {
      return;
    }
    aliasses.push(level1Key);
    _.forOwn(entry, function (entry, level2Key) {
      if ( level2Key !== 'options' ) {
        aliasses.push(level1Key + ':' + level2Key);
      }
    });
  });
  _.forEach(aliasses, function (alias) {
    grunt.registerTask(alias + '-alias', [alias]);
  });

};