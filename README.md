#Grunt aliases

Due to jit-grunt's lazy loading tasks are recognized by grunt GUI tools such as sublime grunt.
Create aliases for tasks to make them available.

## Install

```

npm install git+https://bitbucket.org/everheij/grunt-aliases.git --save-dev
```